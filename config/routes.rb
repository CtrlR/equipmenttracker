EquipmentTracker::Application.routes.draw do

  resources :equipment do
    collection do
      post :import
    end
    resources :loans
    resources :services do
      resources :serviceparts
      resources :serviceimages
    end
    member do
      match 'startoggle' => 'equipment#startoggle'
      match 'invreset' => 'equipment#invreset'
    end
  end
  
  resources :services do
    member do
      match 'serviceRequest' => 'services#serviceRequest'
    end
    resources :serviceparts
    resources :serviceimages
  end
  
  resources :serviceparts do
    resources :parts
  end
  
  resources :serviceimages
  
  resources :parts do
    member do
      match 'invreset' => 'parts#invreset'
    end
  end

  resources :loans
  
  devise_for :users
  
  resources :users do
    member do
      match 'invmodetoggle' => 'users#invmodetoggle'
    end
  end

  get 'about' => 'pages#about'
  
  get 'part' => 'part#index'
  
  get 'archived' => 'equipment#archived'
  
  get 'service' => 'services#index'

  get 'holdlist' => 'equipment#holdlist'    

  get 'openloan' => 'loans#index' 

  get 'equipment' => 'equipment#index'
  
  root :to => 'equipment#index'

  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  # root :to => 'welcome#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
end
