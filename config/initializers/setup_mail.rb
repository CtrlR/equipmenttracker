ActionMailer::Base.smtp_settings = {
  :address              => "smtp.gmail.com",
  :port                 => 587,
  :domain               => "gmail.com",
  :user_name            => ENV["EMAIL_USERNAME"],
  :password             => ENV["EMAIL_PASSWORD"],
  :authentication       => "plain",
  :enable_starttls_auto => true,
}

ActionMailer::Base.default_url_options[:host] = ENV["ACTIONMAILER_URL"]
#Mail.register_interceptor(DevelopmentMailInterceptor) if Rails.env.development?