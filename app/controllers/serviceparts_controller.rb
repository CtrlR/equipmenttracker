class ServicepartsController < ApplicationController
  before_filter :authenticate_user! #, except: [:index]
  # GET /parts
  # GET /parts.json
  def index
    @serviceparts = Servicepart.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @serviceparts }
    end
  end

  # GET /parts/1
  # GET /parts/1.json
  def show
    @servicepart = Servicepart.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @servicepart }
    end
  end

  # GET /parts/new
  # GET /parts/new.json
  def new
    @servicepart = Servicepart.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @servicepart }
    end
  end

  # GET /parts/1/edit
  def edit
    @servicepart = Servicepart.find(params[:id])
    @parts = Part.all
  end

  # POST /parts
  # POST /parts.json
  def create
    @service = Service.find(params[:service_id])
    @currentlaborcost = @service.laborcost
    @currentmaterialcost = @service.materialcost
    @servicepart = @service.serviceparts.create(params[:servicepart])
    @part = Part.find(@servicepart.part_id)
    @servicepartcostTotal = Part.find(@servicepart.part_id).cost*@servicepart.qty
    @service.materialcost = (if !@service.materialcost.blank? then @currentmaterialcost else 0.00 end) + @servicepartcostTotal
    @service.servicecost = ((if !@service.materialcost.blank? then @currentmaterialcost else 0.00 end) + @servicepartcostTotal) + @currentlaborcost
    @part.onhand = (if !@part.onhand.blank? then @part.onhand else 0 end) - @servicepart.qty
    @part.save
    @service.save
    redirect_to service_path(@service)
  end

  # PUT /parts/1
  # PUT /parts/1.json
  def update
    @servicepart = Servicepart.find(params[:id])
    @service = Service.find(@servicepart.service_id)
    @part = Part.find_by_id(@servicepart.part_id)
    if Part.find_by_id(@servicepart.part_id) then
      @previousservicepartcostTotal = @part.cost*@servicepart.qty
    else
      @previousservicepartcostTotal = 0
    end
    @previousqty = @servicepart.qty

    respond_to do |format|
      if @servicepart.update_attributes(params[:servicepart])
        @currentlaborcost = @service.laborcost
        if Part.find_by_id(@servicepart.part_id) then
          @currentservicepartcostTotal = @part.cost*@servicepart.qty
        else
          @currentservicepartcostTotal = 0
        end
        @service.materialcost = (if !@service.materialcost.blank? then @previousservicepartcostTotal + (@currentservicepartcostTotal - @previousservicepartcostTotal)  else @currentservicepartcostTotal end)
        @service.servicecost = (if !@service.materialcost.blank? then (@previousservicepartcostTotal + (@currentservicepartcostTotal - @previousservicepartcostTotal) + @currentlaborcost)  else (@currentservicepartcostTotal + @currentlaborcost) end)
        if Part.find_by_id(@servicepart.part_id) then
          @part.onhand = (if !@part.onhand.blank? then @part.onhand - (@servicepart.qty - @previousqty) else 0 end)
          @part.save
        else
        end
        @service.save
        format.html { redirect_to @service, notice: 'Service part was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @servicepart.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /parts/1
  # DELETE /parts/1.json
  def destroy
    @servicepart = Servicepart.find(params[:id])
    @service = Service.find(@servicepart.service_id)
    @part = Part.find_by_id(@servicepart.part_id)
    if Part.find_by_id(@servicepart.part_id) then
      @servicepartcostTotal = @part.cost*@servicepart.qty
    else
      @servicepartcostTotal = 0
    end
    @service.materialcost = (if !@service.materialcost.blank? then @service.materialcost - @servicepartcostTotal  else 0.00 end)
    @service.servicecost = (if !@service.servicecost.blank? then @service.servicecost - @servicepartcostTotal  else 0.00 end)
    if Part.find_by_id(@servicepart.part_id) then
      @part.onhand = (if !@part.onhand.blank? then @part.onhand + @servicepart.qty else 0 end)
      @part.save
    else
    end
    @service.save
    @servicepart.destroy

    respond_to do |format|
      format.html { redirect_to service_url(@service) }
      format.json { head :no_content }
    end
  end
end
