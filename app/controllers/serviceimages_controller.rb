class ServiceimagesController < ApplicationController
  before_filter :authenticate_user! #, except: [:index]
  # GET /parts
  # GET /parts.json
  def index
    @serviceimages = Serviceimage.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @serviceimages }
    end
  end

  # GET /parts/1
  # GET /parts/1.json
  def show
    @serviceimage = Serviceimage.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @serviceimage }
    end
  end

  # GET /parts/new
  # GET /parts/new.json
  def new
    @serviceimage = Serviceimage.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @serviceimage }
    end
  end

  # GET /parts/1/edit
  def edit
    @serviceimage = Serviceimage.find(params[:id])
  end

  # POST /parts
  # POST /parts.json
  def create
    @service = Service.find(params[:service_id])
    @serviceimage = @service.serviceimages.create(params[:serviceimage])
    redirect_to service_path(@service)
  end

  # PUT /parts/1
  # PUT /parts/1.json
  def update
    @serviceimage = Serviceimage.find(params[:id])
    respond_to do |format|
      if @serviceimage.update_attributes(params[:serviceimage])
        format.html { redirect_to @serviceimage.service, notice: 'Service image was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @serviceimage.service.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /parts/1
  # DELETE /parts/1.json
  def destroy
    @serviceimage = Serviceimage.find(params[:id])
    @service = @serviceimage.service_id
    @serviceimage.destroy

    respond_to do |format|
      format.html { redirect_to service_url(@service) }
      format.json { head :no_content }
    end
  end
end
