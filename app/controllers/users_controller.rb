class UsersController < ApplicationController
  before_filter :authenticate_user! #, except: [:index]
  
  def invmodetoggle
    @user = User.find(params[:id])
    if @user.invmode?
      if @user.invmode == FALSE
        @user.invmode = true
        @user.save
        redirect_to root_url, notice: 'Inventory mode has been turned ON.'
      else
        @user.invmode = false
        @user.save
        redirect_to root_url, notice: 'Inventory mode has been turned OFF.'
      end
    else
      @user.invmode = true
      @user.save
      redirect_to root_url, notice: 'Inventory mode has been turned ON.'      
    end
  end

end
