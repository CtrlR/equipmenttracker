class ServicesController < ApplicationController
  before_filter :authenticate_user! #, except: [:index]
  # GET /services
  # GET /services.json
  helper_method :sort_column, :sort_direction
  
  def index
    @services = Service.order(sort_column + " " + sort_direction).page(params[:page]).per_page(20).search(params[:search])

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @services }
    end
  end

  # GET /services/1
  # GET /services/1.json
  def show
    @service = Service.find(params[:id])
    @parts = Part.all

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @service }
    end
  end

  # GET /services/new
  # GET /services/new.json
  def new
    @service = Service.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @service }
    end
  end

  # GET /services/1/edit
  def edit
    @service = Service.find(params[:id])
  end

  # POST /services
  # POST /services.json
  def create
    if params[:commit] == "Add Service Record"
      @equipment = Equipment.find(params[:equipment_id])
      @service = @equipment.services.create(params[:service])
      @service.materialcost = (if !@service.materialcost.blank? then @service.materialcost else 0.00 end)
      @service.laborcost = (if !@service.laborcost.blank? then @service.laborcost else 0.00 end)
      @service.servicecost = (if !@service.laborcost.blank? then @service.laborcost else 0.00 end) + (if !@service.materialcost.blank? then @service.materialcost else 0.00 end)
      @service.save
      redirect_to equipment_path(@equipment, :scan => 0), notice: 'Service entry has been recorded.'
    else
      @equipment = Equipment.find(params[:equipment_id])
      @service = @equipment.services.create(params[:service])
      holdnotes = "Service requested by " + current_user.name + " on " + Date.today.to_s + ": " + @service.serviceperformed
      @techemail = ENV["TECH_EMAIL"]
      @user = current_user
      @equipment.hold=true
      @equipment.holdnotes=holdnotes
      @service.materialcost = (if !@service.materialcost.blank? then @service.materialcost else 0.00 end)
      @service.laborcost = (if !@service.laborcost.blank? then @service.laborcost else 0.00 end)
      @service.servicecost = (if !@service.laborcost.blank? then @service.laborcost else 0.00 end) + (if !@service.materialcost.blank? then @service.materialcost else 0.00 end)
      @service.save
      @equipment.save
      if user_signed_in?
        UserMailer.request_service(@equipment, @techemail, @user, @service).deliver
        redirect_to equipment_path(@equipment, :scan => 0), notice: 'Service request has been sent.'
      else
      end
    end
  end

  # PUT /services/1
  # PUT /services/1.json
  def update
    @service = Service.find(params[:id])

    respond_to do |format|
      if @service.update_attributes(params[:service])
        @service.servicecost = (if !@service.laborcost.blank? then @service.laborcost else 0.00 end) + (if !@service.materialcost.blank? then @service.materialcost else 0.00 end)
        @service.save
        format.html { redirect_to @service, notice: 'Service was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @service.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /services/1
  # DELETE /services/1.json
  def destroy
    @service = Service.find(params[:id])
    @service.destroy
    redirect_to equipment_path(@service.equipment_id, :scan => 0)
  end
  
  def serviceRequest
    @techemail = "darrell@hls2.com"
    @user = current_user
    @equipment = Equipment.find(params[:id])
    @equipment.hold=true
    @equipment.save
    if user_signed_in?
      UserMailer.request_service(@equipment, @techemail, @user).deliver
      redirect_to equipment_path(@equipment, :scan => 0), notice: 'Service request has been sent.'
    else
    end
  end
  
  private
  
  def sort_column
    Service.column_names.include?(params[:sort]) ? params[:sort] : "servicedate"
  end
  
  def sort_direction
    %w[asc desc].include?(params[:direction]) ? params[:direction] : "desc"
  end
  
end
