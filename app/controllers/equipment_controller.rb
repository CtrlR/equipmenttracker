class EquipmentController < ApplicationController
  before_filter :authenticate_user! #, except: [:index]
  # GET /equipment
  # GET /equipment.json
  helper_method :sort_column, :sort_direction, :filter_equipmenttype
  
  def index
    if filter_equipmenttype.blank?
      @equipment = Equipment.active.order(sort_column + " " + sort_direction).page(params[:page]).per_page(20).search(params[:search])
    else
      @equipment = Equipment.active.order(sort_column + " " + sort_direction).page(params[:page]).per_page(20).where(equipmenttype: filter_equipmenttype).search(params[:search])
    end
    
    respond_to do |format|
      format.html # index.html.erb
      format.csv {if filter_equipmenttype.blank?
        @equipment = Equipment.active.order(sort_column + " " + sort_direction).search(params[:search])
      else
        @equipment = Equipment.active.order(sort_column + " " + sort_direction).where(equipmenttype: filter_equipmenttype).search(params[:search])
      end}
      format.xls {if filter_equipmenttype.blank?
        @equipment = Equipment.active.order(sort_column + " " + sort_direction).search(params[:search])
      else
        @equipment = Equipment.active.order(sort_column + " " + sort_direction).where(equipmenttype: filter_equipmenttype).search(params[:search])
      end}
      format.json { render json: @equipment }
    end
  end
  
  def archived
    if filter_equipmenttype.blank?
      @equipment = Equipment.inactive.order(sort_column + " " + sort_direction).page(params[:page]).per_page(20).search(params[:search])
    else
      @equipment = Equipment.inactive.order(sort_column + " " + sort_direction).page(params[:page]).per_page(20).where(equipmenttype: filter_equipmenttype).search(params[:search])
    end
    
    respond_to do |format|
      format.html # archived.html.erb
      format.json { render json: @equipment }
    end
  end

  # GET /equipment/1
  # GET /equipment/1.json
  def show
    @equipment = Equipment.find(params[:id])
    if !@equipment.services.blank?
      @lastservicehours = Equipment.find(params[:id]).services.where('operatinghours IS NOT NULL').order('servicedate ASC').last
      @lastservicemiles = Equipment.find(params[:id]).services.where('mileage IS NOT NULL').order('servicedate ASC').last
    end
    if params[:download]
      send_data(render_to_string, :filename => "#{@equipment.barcode}-label.html", :type => "text/html")
    else
  
      respond_to do |format|
        format.html # show.html.erb
        format.json { render json: @equipment }
      end
    end
  end

  # GET /equipment/new
  # GET /equipment/new.json
  def new
    @equipment = current_user.equipment.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @equipment }
    end
  end

  # GET /equipment/1/edit
  def edit
    @equipment = Equipment.find(params[:id])
  end

  # POST /equipment
  # POST /equipment.json
  def create
    @equipment = current_user.equipment.new(params[:equipment])

    respond_to do |format|
      if @equipment.save
        if params[:commit] == "Add/Update Equipment"
          format.html { redirect_to new_equipment_path, notice: 'Equipment was successfully created.' }
        else
          format.html { redirect_to equipment_path(@equipment), notice: 'Equipment was successfully created. Click the QR Code button to download the label file.' }
        end  
        format.json { render json: @equipment, status: :created, location: @equipment }
      else
        format.html { render action: "new" }
        format.json { render json: @equipment.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /equipment/1
  # PUT /equipment/1.json
  def update
    @equipment = Equipment.find(params[:id])

    respond_to do |format|
      if @equipment.update_attributes(params[:equipment])
        format.html { redirect_to @equipment, notice: 'Equipment was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @equipment.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /equipment/1
  # DELETE /equipment/1.json
  def destroy
    @equipment = Equipment.find(params[:id])
    if @equipment.archived.blank?
      @equipment.archived = true
      @equipment.save
    else
      @equipment.destroy
    end

    respond_to do |format|
      format.html { redirect_to equipment_index_url }
      format.json { head :no_content }
    end
  end
  
  def holdlist
    @equipment = Equipment.order(sort_column + " " + sort_direction).page(params[:page]).per_page(20).where(:hold => true)

    respond_to do |format|
      format.html # holdlist.html.erb
      format.json { render json: @equipment }
    end
  end
  
  def startoggle
    @equipment = Equipment.find(params[:id])
    holdnotes = "Placed on hold by " + current_user.name + " on " + Date.today.to_s
    if user_signed_in?
      if !@equipment.hold.blank?
        if @equipment.hold=false
          @equipment.hold=true
          @equipment.holdnotes=holdnotes
          @equipment.holddate=Date.today
          @equipment.save
          redirect_to Rails.cache.read("whereyouare"), notice: 'Item has been placed in hold list.'
        else
          @equipment.hold=false
          @equipment.holdnotes=""
          @equipment.holddate=nil
          @equipment.save
          redirect_to Rails.cache.read("whereyouare"), notice: 'Item has been removed from hold list.'
        end
      else
        @equipment.hold=true
        @equipment.holdnotes=holdnotes
        @equipment.holddate=Date.today
        @equipment.save
        redirect_to Rails.cache.read("whereyouare"), notice: 'Item has been placed in hold list.'
      end
    end
  end
  
  def invreset
    Equipment.update_all(:invcount => 0)
    Equipment.update_all(:invuser => nil)
    Equipment.update_all(:invscandate => nil)
    redirect_to root_url, notice: 'All inventory counts reset to zero.'
  end
  
  def import
    Equipment.import(params[:file])
    redirect_to root_url, notice: "Equipment imported."
  end
  
  private
  
  def sort_column
    Equipment.column_names.include?(params[:sort]) ? params[:sort] : "unitname"
  end
  
  def sort_direction
    %w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
  end
  
  def filter_equipmenttype
    Equipment.uniq.pluck(:equipmenttype).include?(params[:typefilter]) ? params[:typefilter] : nil
  end
end
