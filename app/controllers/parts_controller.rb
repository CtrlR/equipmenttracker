class PartsController < ApplicationController
  before_filter :authenticate_user! #, except: [:index]
  # GET /parts
  # GET /parts.json
  helper_method :sort_column, :sort_direction, :filter_category
  
  def index
    if filter_category.blank?
      @parts = Part.order(sort_column + " " + sort_direction).page(params[:page]).per_page(20).search(params[:search])
    else
      @parts = Part.order(sort_column + " " + sort_direction).page(params[:page]).per_page(20).where(category: filter_category).search(params[:search])
    end

    respond_to do |format|
      format.html # index.html.erb
      format.csv {if filter_category.blank?
        @parts = Part.order(sort_column + " " + sort_direction).search(params[:search])
      else
        @parts = Part.order(sort_column + " " + sort_direction).where(category: filter_category).search(params[:search])
      end}
      format.xls {if filter_category.blank?
        @parts = Part.order(sort_column + " " + sort_direction).search(params[:search])
      else
        @parts = Part.order(sort_column + " " + sort_direction).where(category: filter_category).search(params[:search])
      end}
      format.json { render json: @parts }
    end
  end

  # GET /parts/1
  # GET /parts/1.json
  def show
    @part = Part.find(params[:id])

    if params[:download]
      send_data(render_to_string, :filename => "#{@part.partnumber}-label.html", :type => "text/html")
    else

      respond_to do |format|
        format.html # show.html.erb
        format.json { render json: @part }
      end
    end
  end

  # GET /parts/new
  # GET /parts/new.json
  def new
    @part = Part.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @part }
    end
  end

  # GET /parts/1/edit
  def edit
    @part = Part.find(params[:id])
  end

  # POST /parts
  # POST /parts.json
  def create
    @part = Part.new(params[:part])

    respond_to do |format|
      if @part.save
        format.html { redirect_to new_part_path, notice: 'Part was successfully created.' }
        format.json { render json: @part, status: :created, location: @part }
      else
        format.html { render action: "new" }
        format.json { render json: @part.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /parts/1
  # PUT /parts/1.json
  def update
    @part = Part.find(params[:id])

    respond_to do |format|
      if @part.update_attributes(params[:part])
        format.html { redirect_to @part, notice: 'Part was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @part.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /parts/1
  # DELETE /parts/1.json
  def destroy
    @part = Part.find(params[:id])
    @part.destroy

    respond_to do |format|
      format.html { redirect_to parts_url }
      format.json { head :no_content }
    end
  end
  
  def invreset
    Part.update_all(:invcount => 0)
    Part.update_all(:invuser => nil)
    Part.update_all(:invscandate => nil)
    redirect_to parts_url, notice: 'All inventory counts reset to zero.'
  end
  
  def sort_column
    Part.column_names.include?(params[:sort]) ? params[:sort] : "name"
  end
  
  def sort_direction
    %w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
  end
  
  def filter_category
    Part.uniq.pluck(:category).include?(params[:typefilter]) ? params[:typefilter] : nil
  end
end
