class LoansController < ApplicationController
  before_filter :authenticate_user! #, except: [:index]
  # GET /loans
  # GET /loans.json
  helper_method :sort_column, :sort_direction
  def index
    @loans = Loan.order(sort_column + " " + sort_direction).page(params[:page]).per_page(20).where("returndate IS NULL")

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @loans }
    end
  end

  # GET /loans/1
  # GET /loans/1.json
  def show
    @loan = Loan.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @loan }
    end
  end

  # GET /loans/new
  # GET /loans/new.json
  def new
    @loan = Loan.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @loan }
    end
  end

  # GET /loans/1/edit
  def edit
    @loan = Loan.find(params[:id])
  end

  # POST /loans
  # POST /loans.json
  def create
    @equipment = Equipment.find(params[:equipment_id])
    @loan = @equipment.loans.create(params[:loan])
    redirect_to equipment_path(@equipment, :scan => 0)
  end

  # PUT /loans/1
  # PUT /loans/1.json
  def update
    @loan = Loan.find(params[:id])

    respond_to do |format|
      if @loan.update_attributes(params[:loan])
        format.html { redirect_to @loan, notice: 'Loan was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @loan.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /loans/1
  # DELETE /loans/1.json
  def destroy
    @loan = Loan.find(params[:id])
    @equipment = Equipment.find(@loan.equipment_id)
    @loan.destroy

    respond_to do |format|
      format.html { redirect_to equipment_path(@equipment, :scan => 0) }
      format.json { head :no_content }
    end
  end
  
    private
  
  def sort_column
    Loan.column_names.include?(params[:sort]) ? params[:sort] : "loandate"
  end
  
  def sort_direction
    %w[asc desc].include?(params[:direction]) ? params[:direction] : "desc"
  end

end
