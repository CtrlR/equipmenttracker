class UserMailer < ActionMailer::Base
  default :from => "ian@hls2.com"

  def request_service(equipment, techemail, user, service)
    @equipment = equipment
    @techemail = techemail
    @user = user
    @service = service
    mail(:to => "#{@techemail} <#{@techemail}>", :reply_to => "#{@user.email}", :from => "#{@user.email}", :subject => "Service has been requested on #{@equipment.unitname}")
  end

end