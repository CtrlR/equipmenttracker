class Service < ActiveRecord::Base
  attr_accessible :mileage, :notes, :operatinghours, :servicedate, :serviceduration, :serviceperformed, :servicecost, :laborcost, :materialcost, :completed

  has_many :serviceparts, dependent: :destroy
  has_many :serviceimages, dependent: :destroy
  belongs_to :equipment
  has_paper_trail
  
  def self.search(search)
    if search
      find(:all, :order => 'servicedate desc', :conditions => ['lower(serviceperformed) LIKE lower(?) OR lower(notes) LIKE lower(?)', "%#{search}%", "%#{search}%"])
    else
      find(:all, :order => 'servicedate desc')
    end
  end
  
  def servicepartcostTotal
    serviceparts.map do |i|
      if !i.part_id.blank?
        then Part.find(i.part_id).cost*i.qty
      else servicepartcostTotal=0.00
      end
    end.sum
  end
  
end
