class Servicepart < ActiveRecord::Base
  attr_accessible :qty, :part_id, :service_id
  
  has_paper_trail
  
  validates_presence_of :part_id
  
  belongs_to :service
  has_one :part
end
