class Equipment < ActiveRecord::Base
  attr_accessible :barcode, :crew, :description, :dop, :equipmenttype, :fueltype, :manufacturer, :performance, :responsible, :serialnumber, :storagelocation, :supplier, :unitname, :image, :hold, :holddate, :archived, :inservicedate, :counter, :invcount, :invuser, :invscandate, :gpsurl
  
  has_attached_file :image, styles: { thumb: "150x150>"}
  has_paper_trail
  
  validates :barcode, presence: true
  validates :user_id, presence: true
  validates_attachment :image, content_type: { content_type: ['image/jpeg', 'image/jpg', 'image/png', 'image/gif']},
                               size: { less_than: 5.megabytes }
  
  belongs_to :user
  has_many :services, dependent: :destroy
  has_many :loans, dependent: :destroy
  
  scope :active, where('archived IS NULL or archived=(?)', false)
  scope :inactive, where('archived=(?)', true)
  
  def self.search(search)
    if search
      find(:all, :conditions => ['lower(barcode) LIKE lower(?) OR lower(description) LIKE lower(?) OR lower(unitname) LIKE lower(?)', "%#{search}%", "%#{search}%", "%#{search}%"])
    else
      find(:all)
    end
  end
  
  def servicecostTotal
    services.map do |i|
      if !i.servicecost.blank?
        then i.servicecost
      else servicecostTotal=0.00
      end
    end.sum
  end
  
  def self.import(file)
    CSV.foreach(file.path, headers: true) do |row|
      Equipment.create! row.to_hash
    end
  end
  
end
