class Loan < ActiveRecord::Base
  attr_accessible :loandate, :loanedto, :loanedby, :returndate
  
  has_paper_trail
  
  validates_presence_of :loanedto, :loanedby, :loandate
  
  belongs_to :equipment
  belongs_to :user
end