class Serviceimage < ActiveRecord::Base
  attr_accessible :image, :caption, :service_id
  
  has_paper_trail
  
  has_attached_file :image, :styles => { :thumb => { :geometry => "150x150>", :format => :jpg } }
  validates_attachment :image, content_type: { content_type: ['image/jpeg', 'image/jpg', 'image/png', 'image/gif', 'application/pdf']},
                               size: { less_than: 5.megabytes }
  
  belongs_to :service
end
