class Part < ActiveRecord::Base
  attr_accessible :category, :description, :location, :name, :partnumber, :cost, :onhand
  
  validates_presence_of :cost, :name, :partnumber, :onhand
  
  has_many :serviceparts
  has_paper_trail
  
  def self.search(search)
    if search
      find(:all, :conditions => ['lower(name) LIKE lower(?) OR lower(partnumber) LIKE lower(?) OR lower(description) LIKE lower(?)', "%#{search}%", "%#{search}%", "%#{search}%"])
    else
      find(:all)
    end
  end
  
end
