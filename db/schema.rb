# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20150218153201) do

  create_table "active_admin_comments", :force => true do |t|
    t.string   "resource_id",   :null => false
    t.string   "resource_type", :null => false
    t.integer  "author_id"
    t.string   "author_type"
    t.text     "body"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
    t.string   "namespace"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], :name => "index_active_admin_comments_on_author_type_and_author_id"
  add_index "active_admin_comments", ["namespace"], :name => "index_active_admin_comments_on_namespace"
  add_index "active_admin_comments", ["resource_type", "resource_id"], :name => "index_admin_notes_on_resource_type_and_resource_id"

  create_table "admin_users", :force => true do |t|
    t.string   "email",                  :default => "", :null => false
    t.string   "encrypted_password",     :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
  end

  add_index "admin_users", ["email"], :name => "index_admin_users_on_email", :unique => true
  add_index "admin_users", ["reset_password_token"], :name => "index_admin_users_on_reset_password_token", :unique => true

  create_table "equipment", :force => true do |t|
    t.string   "barcode"
    t.string   "unitname"
    t.string   "manufacturer"
    t.string   "equipmenttype"
    t.string   "serialnumber"
    t.string   "fueltype"
    t.date     "dop"
    t.integer  "performance"
    t.string   "description"
    t.string   "storagelocation"
    t.string   "crew"
    t.string   "responsible"
    t.string   "supplier"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
    t.integer  "user_id"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.boolean  "hold"
    t.date     "inservicedate"
    t.integer  "counter"
    t.integer  "invcount"
    t.string   "invuser"
    t.date     "invscandate"
    t.boolean  "archived"
    t.string   "holdnotes"
    t.string   "gpsurl"
    t.date     "holddate"
  end

  add_index "equipment", ["user_id"], :name => "index_equipment_on_user_id"

  create_table "loans", :force => true do |t|
    t.date     "loandate"
    t.string   "loanedto"
    t.string   "loanedby"
    t.date     "returndate"
    t.integer  "equipment_id"
    t.integer  "user_id"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  add_index "loans", ["equipment_id"], :name => "index_loans_on_equipment_id"
  add_index "loans", ["user_id"], :name => "index_loans_on_user_id"

  create_table "parts", :force => true do |t|
    t.string   "partnumber"
    t.string   "name"
    t.text     "description"
    t.string   "category"
    t.string   "location"
    t.decimal  "cost",           :precision => 8, :scale => 2
    t.integer  "onhand"
    t.integer  "servicepart_id"
    t.datetime "created_at",                                   :null => false
    t.datetime "updated_at",                                   :null => false
    t.string   "invuser"
    t.date     "invscandate"
    t.integer  "invcount"
  end

  add_index "parts", ["servicepart_id"], :name => "index_parts_on_servicepart_id"

  create_table "serviceimages", :force => true do |t|
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.string   "caption"
    t.integer  "service_id"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
  end

  add_index "serviceimages", ["service_id"], :name => "index_serviceimages_on_service_id"

  create_table "serviceparts", :force => true do |t|
    t.integer  "part_id"
    t.integer  "qty"
    t.integer  "service_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "serviceparts", ["part_id"], :name => "index_serviceparts_on_part_id"
  add_index "serviceparts", ["service_id"], :name => "index_serviceparts_on_service_id"

  create_table "services", :force => true do |t|
    t.string   "barcode"
    t.string   "serviceperformed"
    t.date     "servicedate"
    t.integer  "operatinghours"
    t.integer  "mileage"
    t.string   "serviceduration"
    t.text     "notes"
    t.integer  "equipment_id"
    t.datetime "created_at",                                     :null => false
    t.datetime "updated_at",                                     :null => false
    t.decimal  "servicecost",      :precision => 8, :scale => 2
    t.integer  "servicepart_id"
    t.integer  "serviceimage_id"
    t.boolean  "completed"
    t.decimal  "laborcost",        :precision => 8, :scale => 2
    t.decimal  "materialcost",     :precision => 8, :scale => 2
  end

  add_index "services", ["equipment_id"], :name => "index_services_on_equipment_id"
  add_index "services", ["serviceimage_id"], :name => "index_services_on_serviceimage_id"
  add_index "services", ["servicepart_id"], :name => "index_services_on_servicepart_id"

  create_table "users", :force => true do |t|
    t.string   "email",                  :default => "",    :null => false
    t.string   "encrypted_password",     :default => "",    :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                                :null => false
    t.datetime "updated_at",                                :null => false
    t.string   "name"
    t.boolean  "invmode",                :default => false
  end

  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true

  create_table "versions", :force => true do |t|
    t.string   "item_type",  :null => false
    t.integer  "item_id",    :null => false
    t.string   "event",      :null => false
    t.string   "whodunnit"
    t.text     "object"
    t.datetime "created_at"
  end

  add_index "versions", ["item_type", "item_id"], :name => "index_versions_on_item_type_and_item_id"

end
