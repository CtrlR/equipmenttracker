class AddColumnHoldToEquipment < ActiveRecord::Migration
  def change
    add_column :equipment, :hold, :boolean
  end
end
