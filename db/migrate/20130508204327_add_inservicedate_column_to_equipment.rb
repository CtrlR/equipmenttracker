class AddInservicedateColumnToEquipment < ActiveRecord::Migration
  def change
    add_column :equipment, :inservicedate, :date
  end
end
