class AddInvColumnsToParts < ActiveRecord::Migration
  def change
    add_column :parts, :invuser, :string
    add_column :parts, :invscandate, :date
    add_column :parts, :invcount, :integer
  end
end
