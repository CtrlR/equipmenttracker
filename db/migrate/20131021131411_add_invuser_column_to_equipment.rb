class AddInvuserColumnToEquipment < ActiveRecord::Migration
  def change
    add_column :equipment, :invuser, :string
    add_column :equipment, :invscandate, :date
  end
end
