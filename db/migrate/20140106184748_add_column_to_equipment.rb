class AddColumnToEquipment < ActiveRecord::Migration
  def change
    add_column :equipment, :archived, :boolean
  end
end
