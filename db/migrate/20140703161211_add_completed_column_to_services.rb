class AddCompletedColumnToServices < ActiveRecord::Migration
  def change
    add_column :services, :completed, :boolean
  end
end
