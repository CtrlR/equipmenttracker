class CreateEquipment < ActiveRecord::Migration
  def change
    create_table :equipment do |t|
      t.string :barcode
      t.string :unitname
      t.string :manufacturer
      t.string :equipmenttype
      t.string :serialnumber
      t.string :fueltype
      t.date :dop
      t.integer :performance
      t.string :description
      t.string :storagelocation
      t.string :crew
      t.string :responsible
      t.string :supplier

      t.timestamps
    end
  end
end
