class AddServiceimageIdToServices < ActiveRecord::Migration
  def change
    add_column :services, :serviceimage_id, :integer
    add_index :services, :serviceimage_id
  end
end
