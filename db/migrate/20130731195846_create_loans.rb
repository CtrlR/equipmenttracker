class CreateLoans < ActiveRecord::Migration
  def change
    create_table :loans do |t|
      t.date :loandate
      t.string :loanedto
      t.string :loanedby
      t.date :returndate
      t.references :equipment
      t.references :user
      
      t.timestamps
    end
    add_index :loans, :equipment_id
    add_index :loans, :user_id
  end
end
