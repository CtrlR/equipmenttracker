class AddGpsurlColumnToEquipment < ActiveRecord::Migration
  def change
    add_column :equipment, :gpsurl, :string
  end
end
