class AddHolddateToEquipment < ActiveRecord::Migration
  def change
    add_column :equipment, :holddate, :date
  end
end
