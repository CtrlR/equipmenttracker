class AddInvmodeColumnToUsers < ActiveRecord::Migration
  def change
    add_column :users, :invmode, :boolean, :default => FALSE
  end
end
