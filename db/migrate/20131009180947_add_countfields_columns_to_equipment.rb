class AddCountfieldsColumnsToEquipment < ActiveRecord::Migration
  def change
    add_column :equipment, :counter, :integer
    add_column :equipment, :invcount, :integer
  end
end
