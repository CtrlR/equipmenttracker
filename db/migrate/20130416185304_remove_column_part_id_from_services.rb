class RemoveColumnPartIdFromServices < ActiveRecord::Migration
  def change
    remove_column :services, :part_id
  end
end
