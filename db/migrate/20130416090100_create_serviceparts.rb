class CreateServiceparts < ActiveRecord::Migration
  def change
    create_table :serviceparts do |t|
      t.references :part
      t.integer :qty
      t.references :service

      t.timestamps
    end
    add_index :serviceparts, :service_id
    add_index :serviceparts, :part_id
  end
end
