class CreateServiceimages < ActiveRecord::Migration
  def change
    create_table :serviceimages do |t|
      t.attachment :image
      t.string :caption
      t.references :service

      t.timestamps
    end
    add_index :serviceimages, :service_id
  end
end
