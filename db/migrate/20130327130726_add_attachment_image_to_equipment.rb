class AddAttachmentImageToEquipment < ActiveRecord::Migration
  def self.up
    change_table :equipment do |t|
      t.attachment :image
    end
  end

  def self.down
    drop_attached_file :equipment, :image
  end
end
