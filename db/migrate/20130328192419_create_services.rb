class CreateServices < ActiveRecord::Migration
  def change
    create_table :services do |t|
      t.string :barcode
      t.string :serviceperformed
      t.date :servicedate
      t.integer :operatinghours
      t.integer :mileage
      t.string :serviceduration
      t.text :notes
      t.decimal :servicecost, :precision => 8, :scale => 2
      t.references :servicepart
      t.references :equipment

      t.timestamps
    end
    add_index :services, :servicepart_id
    add_index :services, :equipment_id
  end
end
