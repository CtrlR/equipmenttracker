class AddUserIdToEquipment < ActiveRecord::Migration
  def change
    add_column :equipment, :user_id, :integer
    add_index :equipment, :user_id
  end
end
