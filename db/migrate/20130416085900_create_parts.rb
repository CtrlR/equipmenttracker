class CreateParts < ActiveRecord::Migration
  def change
    create_table :parts do |t|
      t.string :partnumber
      t.string :name
      t.text :description
      t.string :category
      t.string :location
      t.decimal :cost, :precision => 8, :scale => 2
      t.integer :onhand
      t.references :servicepart

      t.timestamps
    end
    add_index :parts, :servicepart_id
  end
end
