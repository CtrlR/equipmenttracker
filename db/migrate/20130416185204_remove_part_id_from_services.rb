class RemovePartIdFromServices < ActiveRecord::Migration
  def change
    remove_index :services, :part_id
  end
end
