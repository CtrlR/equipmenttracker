class AddCostColumnsToServices < ActiveRecord::Migration
  def change
    add_column :services, :laborcost, :decimal, :precision => 8, :scale => 2
    add_column :services, :materialcost, :decimal, :precision => 8, :scale => 2
  end
end
