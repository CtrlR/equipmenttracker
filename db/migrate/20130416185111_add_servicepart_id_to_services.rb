class AddServicepartIdToServices < ActiveRecord::Migration
  def change
    add_column :services, :servicepart_id, :integer
    add_index :services, :servicepart_id
  end
end
