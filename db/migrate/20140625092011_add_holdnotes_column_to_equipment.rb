class AddHoldnotesColumnToEquipment < ActiveRecord::Migration
  def change
    add_column :equipment, :holdnotes, :string
  end
end
